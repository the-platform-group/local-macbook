#!/bin/bash

# 1. create SSH key and add to Gitlab
# 2. Clone git repos for PGL

# Homebrew
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"

# Add brew to shell path
(echo; echo 'eval "$(/opt/homebrew/bin/brew shellenv)"') >> /Users/$USER/.zprofile
eval "$(/opt/homebrew/bin/brew shellenv)"

brew install \
  git \
  postgresql@16 \
  ruby@3.2 \
  node@18 \
  jq \
  vim

brew install --cask \
  1password \
  google-chrome \
  iterm2 \
  slack \
  shiftit \
  visual-studio-code
# Note these are non approved apps. I'm disabling them for now to try and figure out
# a bluetooth headset issue
  # docker \
  # recordit \
  # scroll-reverser \

# Git
git config --global push.default current
git config --global branch.autosetupmerge true
git config --global branch.autosetuprebase always
git config --global rebase.autosquash true
git config --global merge.ff only

INIT_FILE=~/.zshrc

touch $INIT_FILE

# Git Extensions
grep -qxF 'source ~/.git-prompt.sh' $INIT_FILE || (curl https://raw.githubusercontent.com/git/git/master/contrib/completion/git-prompt.sh -o ~/.git-prompt.sh && echo "source ~/.git-prompt.sh" >> $INIT_FILE && source $INIT_FILE)
grep -qxF 'fpath=(~/.zsh/functions $fpath)' $INIT_FILE || (curl https://raw.githubusercontent.com/git/git/master/contrib/completion/git-prompt.sh -o ~/.git-prompt.sh && echo 'fpath=(~/.zsh/functions $fpath)' >> $INIT_FILE && source $INIT_FILE)
grep -qxF 'autoload -Uz compinit && compinit' $INIT_FILE || (curl https://raw.githubusercontent.com/git/git/master/contrib/completion/git-prompt.sh -o ~/.git-prompt.sh && echo 'autoload -Uz compinit && compinit' >> $INIT_FILE && source $INIT_FILE)
grep -qxF 'precmd () { __git_ps1 "%n" ":%~$ " "|%s" }' $INIT_FILE || (curl https://raw.githubusercontent.com/git/git/master/contrib/completion/git-prompt.sh -o ~/.git-prompt.sh && echo 'precmd () { __git_ps1 "%n" ":%~$ " "|%s" }' >> $INIT_FILE && source $INIT_FILE)

# VIM
curl -o ~/.vimrc https://gist.githubusercontent.com/simonista/8703722/raw/d08f2b4dc10452b97d3ca15386e9eed457a53c61/.vimrc

# Vim Solarized
mkdir -p ~/.vim/colors
curl -o ~/.vim/colors/solarized.vim https://raw.githubusercontent.com/altercation/vim-colors-solarized/master/colors/solarized.vim
# TODO sed colorscheme to remove comment

# Git PS1
grep -qF 'export PROMPT_COMMAND' $INIT_FILE || echo $'export PROMPT_COMMAND=\'__git_ps1 \"\\n\\w\" \"\\n\\\\\$ \"\'' >> $INIT_FILE
grep -qxF 'export GIT_PS1_SHOWDIRTYSTATE=1' $INIT_FILE || echo 'export GIT_PS1_SHOWDIRTYSTATE=1' >> $INIT_FILE
grep -qxF 'export GIT_PS1_SHOWCOLORHINTS=1' $INIT_FILE || echo 'export GIT_PS1_SHOWCOLORHINTS=1' >> $INIT_FILE
grep -qxF 'export GIT_PS1_SHOWUNTRACKEDFILES=1' $INIT_FILE || echo 'export GIT_PS1_SHOWUNTRACKEDFILES=1' >> $INIT_FILE

# - TODO add pgl

# EDITORS
grep -qxF 'export GIT_EDITOR=vim' $INIT_FILE || echo 'export GIT_EDITOR=vim' >> $INIT_FILE
grep -qxF 'export BUNDLER_EDITOR=code' $INIT_FILE || echo 'export BUNDLER_EDITOR=code' >> $INIT_FILE

# History
grep -qxF 'export HISTFILESIZE=1000000' $INIT_FILE || echo 'export HISTFILESIZE=1000000' >> $INIT_FILE
grep -qxF 'export HISTSIZE=1000000' $INIT_FILE || echo 'export HISTSIZE=1000000' >> $INIT_FILE

# Aliases
curl -o ~/.aliases https://gitlab.com/bradrobertson/local-macbook/raw/main/aliases
grep -qxF 'source ~/.aliases' $INIT_FILE || echo 'source ~/.aliases' >> $INIT_FILE

# Ruby override
grep -qxF 'export PATH="/opt/homebrew/opt/ruby@3.2/bin:$PATH"' $INIT_FILE || echo 'export PATH="/opt/homebrew/opt/ruby@3.2/bin:$PATH"' >> $INIT_FILE
# Rubygems exectuables in path '
touch ~/.gemrc
grep -qxF 'gem: --bindir /opt/homebrew/opt/ruby@3.2/bin --no-rdoc --no-ri' ~/.gemrc || echo 'gem: --bindir /opt/homebrew/opt/ruby@3.2/bin --no-rdoc --no-ri' >> ~/.gemrc

# Node override
grep -qxF 'export PATH="/opt/homebrew/opt/node@18/bin:$PATH"' $INIT_FILE || echo 'export PATH="/opt/homebrew/opt/node@18/bin:$PATH"' >> $INIT_FILE

# PG override
grep -qxF 'export PATH="/opt/homebrew/opt/postgresql@16/bin:$PATH"' $INIT_FILE || echo 'export PATH="/opt/homebrew/opt/postgresql@16/bin:$PATH"' >> $INIT_FILE

# Bash style word boundaries (includes dots,dashes,slashes for word style)
grep -qxF 'autoload -U select-word-style' $INIT_FILE || echo 'autoload -U select-word-style' >> $INIT_FILE
grep -qxF 'select-word-style bash' $INIT_FILE || echo 'select-word-style bash' >> $INIT_FILE

# Psql
curl -o ~/.psqlrc https://gitlab.com/bradrobertson/local-macbook/raw/main/psqlrc

# Shift-it - enable the extra taps to go 1/3, 2/3 sizes
defaults write org.shiftitapp.ShiftIt multipleActionsCycleWindowSizes YES

# TODO Keyboard repeat rates
# defaults write -g InitialKeyRepeat -int 10 # normal minimum is 15 (225 ms)
# defaults write -g KeyRepeat -int 1 # normal minimum is 2 (30 ms)

# TODO Iterm Customizations
# https://www.iterm2.com/documentation-hidden-settings.html

# Manual (for now) Steps to perform
# iTerm -> Profiles -> Colors -> Color Presets -> Solarized Dark
# iTerm -> Profiles -> Colors -> Selection / Selected Text changes
# iTerm -> Profiles -> Keys -> Key Mappings -> Presets -> Natural Text Editing (yes to clear out other presets)

# VSCode extensions
code --install-extension ms-vscode.atom-keybindings
code --install-extension rebornix.Ruby
code --install-extension dsznajder.es7-react-js-snippets
code --install-extension HashiCorp.terraform
code --install-extension ms-azuretools.vscode-docker
code --install-extension eamodio.gitlens

# 1Password Extras
# - Enable SSH Agent: https://developer.1password.com/docs/ssh/get-started
# 